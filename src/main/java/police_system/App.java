package police_system;

import java.sql.SQLException;

import javafx.application.Application;
import police_system.database.DatabaseConnector;
import police_system.gui.GUI;

/**
 * Main class starting FXML Application
 * 
 * @author Mat�� Mah�t
 *
 */
public class App {

	public static void main(String[] args) {

		try {
			new GUI();
			Application.launch(GUI.class, args);
		} finally {
			try {
				DatabaseConnector.disconnect();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
