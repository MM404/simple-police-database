package police_system.util;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Clob;

import javax.imageio.ImageIO;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

public class Converter {

	public static Image convertToJavaFXImage(byte[] raw, final int width, final int height) {
		WritableImage image = new WritableImage(width, height);
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(raw);
			BufferedImage read = ImageIO.read(bis);
			image = SwingFXUtils.toFXImage(read, null);
		} catch (IOException ex) {

		}
		return image;
	}

	public static String clobStringConversion(Clob clb) {
		if (clb == null)
			return "";

		StringBuffer str = new StringBuffer();
		String strng;

		try {
			BufferedReader bufferRead = new BufferedReader(clb.getCharacterStream());

			while ((strng = bufferRead.readLine()) != null)
				str.append(strng);
		} catch (Exception e) {
			return "";
		}
		return str.toString();
	}

}
