package police_system.util;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseUtil {

	public static String resultSetToString(ResultSet result) {
		StringBuilder sb = new StringBuilder();
		try {
			while (result.next()) {
				int i = 1;
				while (i <= result.getMetaData().getColumnCount()) {
					sb.append(result.getString(i));
					i++;
					if (i <= result.getMetaData().getColumnCount())
						sb.append(", ");
				}
				sb.append("\n");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

		return sb.toString();

	}

}
