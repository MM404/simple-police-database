package police_system.gui;

import java.io.IOException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class CaseChooserCont {

	private MainScreenCont mainScreenCont;
	private Stage stage;
	@FXML
	private JFXButton b_submit;
	@FXML
	private JFXTextField f_case;

	public void initialize() {

	}

	public void setMainScreen(MainScreenCont mainScreenCont) {
		this.mainScreenCont = mainScreenCont;

	}

	public void setStage(Stage stage) {
		this.stage = stage;

	}

	public void submit() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("case.fxml"));
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene((Parent) fxmlLoader.getRoot(), 1300, 750);
		Stage stage = new Stage();
		stage.setTitle("Case " + f_case.getText());
		stage.setScene(scene);
		stage.show();
		stage.setResizable(false);

		((CaseCont) (fxmlLoader.getController())).setStage(stage);
		System.out.println(f_case.getText());
		((CaseCont) (fxmlLoader.getController())).setCase(Integer.valueOf(f_case.getText()));
		((CaseCont) (fxmlLoader.getController())).init();

		this.stage.close();
	}

}
