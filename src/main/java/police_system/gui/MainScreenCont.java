package police_system.gui;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Struct;
import java.sql.Types;
import java.time.LocalDate;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import oracle.sql.ARRAY;
import oracle.sql.StructDescriptor;
import police_system.database.DatabaseConnector;
import police_system.util.DatabaseUtil;

/**
 * Controler class for main application stage
 * 
 * @author Mat�� Mah�t
 *
 */

public class MainScreenCont {

	@FXML
	private JFXButton b_empCosts;
	@FXML
	private JFXButton b_crimeOverV;
	@FXML
	private JFXButton b_crimeByArea;
	@FXML
	private JFXButton b_report;
	@FXML
	private JFXButton b_esc;
	@FXML
	private JFXButton b_crimeTypeSumm;
	@FXML
	private JFXButton b_amnesty;
	@FXML
	private JFXButton b_solveRatio;
	@FXML
	private JFXButton b_caseDamage;
	@FXML
	private JFXButton b_solveRatioCommonCrime;
	@FXML
	private JFXButton b_numberOfPrisoners;
	@FXML
	private JFXButton b_caseInfo;
	@FXML
	private AnchorPane pane;
	@FXML
	private JFXTextArea textArea;
	@FXML
	private Label l_title;

	public void initialize() {

	}

	public void employeeCosts() {
		ResultSet result = null;
		try {
			result = DatabaseConnector.query("SELECT SUM(CASE WHEN DATE_TO!=NULL \r\n"
					+ "THEN CASE WHEN EXTRACT(YEAR FROM TO_DATE(DATE_FROM, 'DD-MON-RR'))='60' THEN salary*MONTHS_BETWEEN(DATE_TO,DATE_FROM)\r\n"
					+ "ELSE salary*MONTHS_BETWEEN(DATE_TO,to_date('1-JAN-60','DD-MON_RR')) END \r\n" + "ELSE \r\n"
					+ "CASE WHEN EXTRACT(YEAR FROM TO_DATE(DATE_FROM, 'DD-MON-RR'))='60' THEN salary*MONTHS_BETWEEN(to_date('1-JAN-61','DD-MON_RR'),DATE_FROM)\r\n"
					+ "ELSE salary*12 END \r\n" + "END) as costs\r\n" + "FROM employee\r\n"
					+ "WHERE (date_to = null OR date_to > to_date('31-DEC-60','DD-MON_RR')) AND date_from < to_date('1-JAN-61','DD-MON_RR')");
			l_title.setText("Employee Costs for Year 1960");
			StringBuilder sb = new StringBuilder();
			sb.append("SUM OF WAGES: ");
			sb.append(DatabaseUtil.resultSetToString(result));
			sb.append("�");
			textArea.setText(sb.toString());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void showReport() {

	}

	public void showCrimeOverview(LocalDate from, LocalDate to) {
		java.sql.Date sFrom = java.sql.Date.valueOf(from);
		java.sql.Date sTo = java.sql.Date.valueOf(to);

		try {
			final String typeName = "TS_CRIME_OVERVIEW";
			final String typeTableName = "TT_CRIME_OVERVIEW";

			// Get a description of your type
			final StructDescriptor structDescriptor = StructDescriptor.createDescriptor(typeName.toUpperCase(),
					DatabaseConnector.getConnection());
			final ResultSetMetaData metaData = structDescriptor.getMetaData();

			// Call the procedure
			CallableStatement cs = DatabaseConnector.prepareCall("{?=call getCrimeOverview(?, ?)}");
			// Result is an java.sql.Array.
			cs.registerOutParameter(1, Types.ARRAY, typeTableName);
			cs.setDate(2, sFrom);
			cs.setDate(3, sTo);
			cs.execute();

			StringBuilder sb = new StringBuilder();
			// ...who's elements are java.sql.Structs
			Object[] data = (Object[]) ((ARRAY) cs.getObject(1)).getArray();
			for (Object tmp : data) {
				Struct row = (Struct) tmp;
				// Attributes are index 1 based...
				int idx = 1;
				for (Object attribute : row.getAttributes()) {
					sb.append(metaData.getColumnName(idx) + " = " + attribute + "\n");
					++idx;
				}
				sb.append("---\n");
			}
			cs.close();

			l_title.setText("Crime Overview");

			textArea.setText(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void showCrimesByArea(String area, String id) {

		try {
			final String typeName = "TS_STATS";
			final String typeTableName = "TT_STATS";

			// Get a description of your type
			final StructDescriptor structDescriptor = StructDescriptor.createDescriptor(typeName.toUpperCase(),
					DatabaseConnector.getConnection());
			final ResultSetMetaData metaData = structDescriptor.getMetaData();

			CallableStatement cs = DatabaseConnector.prepareCall("{?=call getCrimeOverview(?, ?)}");
			switch (area) {
			case CrimeByAreaCont.city:
				cs = DatabaseConnector.prepareCall("{?=call getCityStats(?)}");
				break;
			case CrimeByAreaCont.region:
				cs = DatabaseConnector.prepareCall("{?=call getRegionStats(?)}");
				break;
			case CrimeByAreaCont.district:
				cs = DatabaseConnector.prepareCall("{?=call getDistrictStats(?)}");
				break;

			default:
				return;
			}
			// Result is an java.sql.Array.
			cs.registerOutParameter(1, Types.ARRAY, typeTableName);
			cs.setString(2, id);
			cs.execute();

			StringBuilder sb = new StringBuilder();
			// ...who's elements are java.sql.Structs
			Object[] data = (Object[]) ((ARRAY) cs.getObject(1)).getArray();
			for (Object tmp : data) {
				Struct row = (Struct) tmp;
				// Attributes are index 1 based...
				int idx = 1;
				for (Object attribute : row.getAttributes()) {
					sb.append(metaData.getColumnName(idx) + " = " + attribute + "\n");
					++idx;
				}
				sb.append("---\n");
			}
			cs.close();

			l_title.setText("Crime Overview");

			this.textArea.setText(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void showCrimeTypeSummary() {

		try {
			final String typeName = "TS_CRIME_SUMM";
			final String typeTableName = "TT_CRIME_SUMM";

			// Get a description of your type
			final StructDescriptor structDescriptor = StructDescriptor.createDescriptor(typeName.toUpperCase(),
					DatabaseConnector.getConnection());
			final ResultSetMetaData metaData = structDescriptor.getMetaData();

			CallableStatement cs = DatabaseConnector.prepareCall("{?=call getCrimeSummary()}");

			// Result is an java.sql.Array.
			cs.registerOutParameter(1, Types.ARRAY, typeTableName);
			cs.execute();

			StringBuilder sb = new StringBuilder();
			// ...who's elements are java.sql.Structs
			Object[] data = (Object[]) ((ARRAY) cs.getObject(1)).getArray();
			for (Object tmp : data) {
				Struct row = (Struct) tmp;
				// Attributes are index 1 based...
				int idx = 1;
				for (Object attribute : row.getAttributes()) {
					sb.append(metaData.getColumnName(idx) + " = " + attribute + "\n");
					++idx;
				}
				sb.append("---\n");
			}
			cs.close();

			l_title.setText("Crime Overview");

			this.textArea.setText(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void showAmnesty() {

		try {
			final String typeName = "TS_AMNESTY_CANDIDATE";
			final String typeTableName = "TT_AMNESTY_CANDIDATE";

			// Get a description of your type
			final StructDescriptor structDescriptor = StructDescriptor.createDescriptor(typeName.toUpperCase(),
					DatabaseConnector.getConnection());
			final ResultSetMetaData metaData = structDescriptor.getMetaData();

			CallableStatement cs = DatabaseConnector.prepareCall("{?=call getAmnestyCandidates()}");

			// Result is an java.sql.Array.
			cs.registerOutParameter(1, Types.ARRAY, typeTableName);
			cs.execute();

			StringBuilder sb = new StringBuilder();
			// ...who's elements are java.sql.Structs
			Object[] data = (Object[]) ((ARRAY) cs.getObject(1)).getArray();
			for (Object tmp : data) {
				Struct row = (Struct) tmp;
				// Attributes are index 1 based...
				int idx = 1;
				for (Object attribute : row.getAttributes()) {
					sb.append(metaData.getColumnName(idx) + " = " + attribute + "\n");
					++idx;
				}
				sb.append("---\n");
			}
			cs.close();

			l_title.setText("Amnesty Candidates");

			this.textArea.setText(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void showLongTermStats() {

		try {
			final String typeName = "TS_CONVICT_SUMMARY";
			final String typeTableName = "TT_CONVICT_SUMMARY";

			// Get a description of your type
			final StructDescriptor structDescriptor = StructDescriptor.createDescriptor(typeName.toUpperCase(),
					DatabaseConnector.getConnection());
			final ResultSetMetaData metaData = structDescriptor.getMetaData();

			CallableStatement cs = DatabaseConnector.prepareCall("{?=call getLongTermStats(?)}");

			// Result is an java.sql.Array.
			cs.registerOutParameter(1, Types.ARRAY, typeTableName);
			cs.setInt(2, 20);
			cs.execute();

			StringBuilder sb = new StringBuilder();
			// ...who's elements are java.sql.Structs
			Object[] data = (Object[]) ((ARRAY) cs.getObject(1)).getArray();
			for (Object tmp : data) {
				Struct row = (Struct) tmp;
				// Attributes are index 1 based...
				int idx = 1;
				for (Object attribute : row.getAttributes()) {
					sb.append(metaData.getColumnName(idx) + " = " + attribute + "\n");
					++idx;
				}
				sb.append("---\n");
			}
			cs.close();

			l_title.setText("Number of prisoners per year");

			this.textArea.setText(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void showSolvedRatio(LocalDate from, LocalDate to) {
		java.sql.Date sFrom = java.sql.Date.valueOf(from);
		java.sql.Date sTo = java.sql.Date.valueOf(to);

		try {
			final String typeName = "TS_REGION_CLARIFY";
			final String typeTableName = "TT_REGION_CLARIFY";

			// Get a description of your type
			final StructDescriptor structDescriptor = StructDescriptor.createDescriptor(typeName.toUpperCase(),
					DatabaseConnector.getConnection());
			final ResultSetMetaData metaData = structDescriptor.getMetaData();

			CallableStatement cs = DatabaseConnector.prepareCall("{?=call GetRegionClarifyStats(?, ?)}");

			// Result is an java.sql.Array.
			cs.registerOutParameter(1, Types.ARRAY, typeTableName);
			cs.setDate(2, sFrom);
			cs.setDate(3, sTo);
			cs.execute();

			StringBuilder sb = new StringBuilder();
			// ...who's elements are java.sql.Structs
			Object[] data = (Object[]) ((ARRAY) cs.getObject(1)).getArray();
			for (Object tmp : data) {
				Struct row = (Struct) tmp;
				// Attributes are index 1 based...
				int idx = 1;
				for (Object attribute : row.getAttributes()) {
					sb.append(metaData.getColumnName(idx) + " = " + attribute + "\n");
					++idx;
				}
				sb.append("---\n");
			}
			cs.close();

			l_title.setText("Solved Crimes Ratio");

			this.textArea.setText(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void showCommonCrimeRatio() {

		try {
			int crimeId = 6;
			CallableStatement cs = DatabaseConnector.prepareCall("{?=call getSolvedRatioForCrimeType(?)}");

			// Result is an java.sql.Array.
			cs.registerOutParameter(1, Types.INTEGER);
			cs.setInt(2, crimeId);
			cs.execute();
			StringBuilder sb = new StringBuilder(String.valueOf(cs.getInt(1)));

			cs.close();

			l_title.setText("Solved Crime Ratio");

			this.textArea.setText(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void showCaseDamage(String criterium) {

		try {
			String typeName;
			String typeTableName;
			CallableStatement cs = null;
			switch (criterium) {
			case CaseDamageCont.resolved:

				typeName = "TS_DAMAGE_STATS";
				typeTableName = "TT_DAMAGE_STATS";
				cs = DatabaseConnector.prepareCall("{?=call getUnresolvedCasesDamage(?)}");

				break;
			case CaseDamageCont.unresolved:

				typeName = "TS_DAMAGE_STATS";
				typeTableName = "TT_DAMAGE_STATS";
				cs = DatabaseConnector.prepareCall("{?=call getResolvedCasesDamage(?)}");
				break;
			default:
				return;
			}

			// Get a description of your type
			final StructDescriptor structDescriptor = StructDescriptor.createDescriptor(typeName.toUpperCase(),
					DatabaseConnector.getConnection());
			final ResultSetMetaData metaData = structDescriptor.getMetaData();

			// Result is an java.sql.Array.
			cs.registerOutParameter(1, Types.ARRAY, typeTableName);
			cs.execute();

			StringBuilder sb = new StringBuilder();
			// ...who's elements are java.sql.Structs
			Object[] data = (Object[]) ((ARRAY) cs.getObject(1)).getArray();
			for (Object tmp : data) {
				Struct row = (Struct) tmp;
				// Attributes are index 1 based...
				int idx = 1;
				for (Object attribute : row.getAttributes()) {
					sb.append(metaData.getColumnName(idx) + " = " + attribute + "\n");
					++idx;
				}
				sb.append("---\n");
			}
			cs.close();

			l_title.setText("Case Damage");

			this.textArea.setText(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void crimeOverview() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("crimeOverview.fxml"));
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene((Parent) fxmlLoader.getRoot(), 450, 300);
		Stage stage = new Stage();
		stage.setTitle("crimeOverview");
		stage.setScene(scene);
		stage.show();
		stage.setResizable(false);

		((CrimeOverviewCont) (fxmlLoader.getController())).setMainScreen(this);
		((CrimeOverviewCont) (fxmlLoader.getController())).setStage(stage);
	}

	public void crimeByArea() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("crimeByArea.fxml"));
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene((Parent) fxmlLoader.getRoot(), 450, 280);
		Stage stage = new Stage();
		stage.setTitle("Crime by Area");
		stage.setScene(scene);
		stage.show();
		stage.setResizable(false);

		((CrimeByAreaCont) (fxmlLoader.getController())).setMainScreen(this);
		((CrimeByAreaCont) (fxmlLoader.getController())).setStage(stage);
	}

	public void caseDamage() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("caseDamage.fxml"));
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene((Parent) fxmlLoader.getRoot(), 450, 280);
		Stage stage = new Stage();
		stage.setTitle("Case Damage");
		stage.setScene(scene);
		stage.show();
		stage.setResizable(false);

		((CaseDamageCont) (fxmlLoader.getController())).setMainScreen(this);
		((CaseDamageCont) (fxmlLoader.getController())).setStage(stage);
	}

	public void solvedRatio() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("solvedRatio.fxml"));
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene((Parent) fxmlLoader.getRoot(), 450, 300);
		Stage stage = new Stage();
		stage.setTitle("Solved Ratio");
		stage.setScene(scene);
		stage.show();
		stage.setResizable(false);

		((SolvedRatioCont) (fxmlLoader.getController())).setMainScreen(this);
		((SolvedRatioCont) (fxmlLoader.getController())).setStage(stage);
	}

	public void caseInfo() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("caseChooser.fxml"));
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene((Parent) fxmlLoader.getRoot(), 450, 300);
		Stage stage = new Stage();
		stage.setTitle("Choose Case");
		stage.setScene(scene);
		stage.show();
		stage.setResizable(false);

		((CaseChooserCont) (fxmlLoader.getController())).setMainScreen(this);
		((CaseChooserCont) (fxmlLoader.getController())).setStage(stage);
	}

	public void escDragEntered() {
		b_esc.setStyle("-fx-background-color: #c12b00");
	}

	public void escDragExited() {
		b_esc.setStyle("-fx-background-color: #4b636e");
	}

	public void close() {
		// get a handle to the stage
		Stage stage = (Stage) b_esc.getScene().getWindow();
		// close window
		stage.close();
	}
}
