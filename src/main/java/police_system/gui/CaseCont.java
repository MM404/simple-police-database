package police_system.gui;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Struct;
import java.sql.Types;
import java.util.Hashtable;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import oracle.sql.ARRAY;
import oracle.sql.StructDescriptor;
import police_system.database.DatabaseConnector;
import police_system.util.Converter;

public class CaseCont {

	private Stage stage;

	@FXML
	private JFXButton b_testimony;
	@FXML
	private JFXButton b_photo;
	@FXML
	private JFXTextArea perOfInt;
	@FXML
	private JFXTextArea crimes;
	@FXML
	private JFXTextArea info;
	@FXML
	private JFXTextArea suspects;
	@FXML
	private JFXTextArea testamony;
	@FXML
	private ImageView imageView;
	@FXML
	private JFXComboBox<Label> cb_witness;
	@FXML
	private JFXComboBox<Label> cb_perOfInt;

	private int caseId;

	private Hashtable<String, Clob> witnesses;

	public void init() {
		witnesses = new Hashtable<>();
		this.initPerOfInt();
		this.initCrimes();
		this.initInfo();
		this.initSuspects();
		this.initWitnesses();
	}

	private void initWitnesses() {
		//
		try {
			final String typeName = "T_REC_TESTIMONY";
			final String typeTableName = "T_TESTIMONY";

			// Get a description of your type
			final StructDescriptor structDescriptor = StructDescriptor.createDescriptor(typeName.toUpperCase(),
					DatabaseConnector.getConnection());
			final ResultSetMetaData metaData = structDescriptor.getMetaData();

			CallableStatement cs = DatabaseConnector.prepareCall("{?=call getTestimony(?)}");

			// Result is an java.sql.Array.
			cs.registerOutParameter(1, Types.ARRAY, typeTableName);
			cs.setInt(2, caseId);
			cs.execute();

			StringBuilder sb = new StringBuilder();
			// ...who's elements are java.sql.Structs
			Object[] data = (Object[]) ((ARRAY) cs.getObject(1)).getArray();
			for (Object tmp : data) {
				Struct row = (Struct) tmp;
				// Attributes are index 1 based...
				witnesses.put((String) row.getAttributes()[0], (Clob) row.getAttributes()[1]);
				cb_witness.getItems().add(new Label((String) row.getAttributes()[0]));
			}
			cs.close();

			this.suspects.setText(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private void initSuspects() {
		try {
			final String typeName = "TS_POI";
			final String typeTableName = "TT_POI";

			// Get a description of your type
			final StructDescriptor structDescriptor = StructDescriptor.createDescriptor(typeName.toUpperCase(),
					DatabaseConnector.getConnection());
			final ResultSetMetaData metaData = structDescriptor.getMetaData();

			CallableStatement cs = DatabaseConnector.prepareCall("{?=call getSuspects(?)}");

			// Result is an java.sql.Array.
			cs.registerOutParameter(1, Types.ARRAY, typeTableName);
			cs.setInt(2, caseId);
			cs.execute();

			StringBuilder sb = new StringBuilder();
			// ...who's elements are java.sql.Structs
			Object[] data = (Object[]) ((ARRAY) cs.getObject(1)).getArray();
			for (Object tmp : data) {
				Struct row = (Struct) tmp;
				// Attributes are index 1 based...
				int idx = 1;
				for (Object attribute : row.getAttributes()) {
					sb.append(metaData.getColumnName(idx) + " = " + attribute + "\n");
					++idx;
				}
				sb.append("------------------------\n");
			}
			cs.close();

			this.suspects.setText(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private void initInfo() {
		try {
			final String typeName = "TS_CASE_DETAIL";

			// Get a description of your type
			final StructDescriptor structDescriptor = StructDescriptor.createDescriptor(typeName.toUpperCase(),
					DatabaseConnector.getConnection());
			final ResultSetMetaData metaData = structDescriptor.getMetaData();

			CallableStatement cs = DatabaseConnector.prepareCall("{?=call getCaseDetails(?)}");

			// Result is an java.sql.Array.
			cs.registerOutParameter(1, Types.STRUCT, typeName);
			cs.setInt(2, caseId);
			cs.execute();

			StringBuilder sb = new StringBuilder();
			// ...who's elements are java.sql.Structs
			Object data = cs.getObject(1);

			Struct row = (Struct) data;
			// Attributes are index 1 based...
			int idx = 1;
			for (Object attribute : row.getAttributes()) {
				sb.append(metaData.getColumnName(idx) + ": " + attribute + ", ");
				++idx;
			}

			cs.close();

			this.info.setText(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void initCrimes() {
		try {
			final String typeName = "TS_CASE_CRIME";
			final String typeTableName = "TT_CASE_CRIME";

			// Get a description of your type
			final StructDescriptor structDescriptor = StructDescriptor.createDescriptor(typeName.toUpperCase(),
					DatabaseConnector.getConnection());
			final ResultSetMetaData metaData = structDescriptor.getMetaData();

			CallableStatement cs = DatabaseConnector.prepareCall("{?=call getCaseCrimes(?)}");

			// Result is an java.sql.Array.
			cs.registerOutParameter(1, Types.ARRAY, typeTableName);
			cs.setInt(2, caseId);
			cs.execute();

			StringBuilder sb = new StringBuilder();
			// ...who's elements are java.sql.Structs
			Object[] data = (Object[]) ((ARRAY) cs.getObject(1)).getArray();
			for (Object tmp : data) {
				Struct row = (Struct) tmp;
				// Attributes are index 1 based...
				int idx = 1;
				for (Object attribute : row.getAttributes()) {
					sb.append(metaData.getColumnName(idx) + " = " + attribute + "\n");
					++idx;
				}
				sb.append("------------------------\n");
			}
			cs.close();

			this.crimes.setText(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private void initPerOfInt() {
		try {
			final String typeName = "TS_POI";
			final String typeTableName = "TT_POI";

			// Get a description of your type
			final StructDescriptor structDescriptor = StructDescriptor.createDescriptor(typeName.toUpperCase(),
					DatabaseConnector.getConnection());
			final ResultSetMetaData metaData = structDescriptor.getMetaData();

			CallableStatement cs = DatabaseConnector.prepareCall("{?=call getPersonOfInterest(?)}");

			// Result is an java.sql.Array.
			cs.registerOutParameter(1, Types.ARRAY, typeTableName);
			cs.setInt(2, caseId);
			cs.execute();

			StringBuilder sb = new StringBuilder();
			// ...who's elements are java.sql.Structs
			Object[] data = (Object[]) ((ARRAY) cs.getObject(1)).getArray();
			for (Object tmp : data) {
				Struct row = (Struct) tmp;
				// Attributes are index 1 based...
				int idx = 1;
				for (Object attribute : row.getAttributes()) {
					sb.append(metaData.getColumnName(idx) + " = " + attribute + "\n");
					if (metaData.getColumnName(idx).equals("ID_NUMBER")) {
						cb_perOfInt.getItems().add(new Label((String) attribute));
					}
					++idx;
				}
				sb.append("------------------------\n");
			}
			cs.close();

			this.perOfInt.setText(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void checkTestimony() {
		testamony.setText(Converter.clobStringConversion(witnesses.get(cb_witness.getValue().getText())));
	}

	public void checkPhoto() {
		try {
			CallableStatement cs = DatabaseConnector.prepareCall("{?=call getPhoto(?, ?)}");

			// Result is an java.sql.Array.
			cs.registerOutParameter(1, Types.BLOB);
			cs.setString(2, cb_perOfInt.getValue().getText());
			cs.setInt(3, caseId);
			cs.execute();

			imageView.setImage(
					Converter.convertToJavaFXImage(cs.getBlob(1).getBytes(1, (int) cs.getBlob(1).length()), 200, 200));

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void setStage(Stage stage) {
		this.stage = stage;

	}

	public void setCase(int id) {
		this.caseId = id;

	}

}
