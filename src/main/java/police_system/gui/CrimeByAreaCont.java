package police_system.gui;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import police_system.database.DatabaseConnector;

public class CrimeByAreaCont {

	private MainScreenCont mainScreenCont;

	public final static String city = "CITY";
	public final static String region = "REGION";
	public final static String district = "DISTRICT";
	private Stage stage;
	@FXML
	private JFXButton b_submit;
	@FXML
	private JFXComboBox<Label> cb_option;
	@FXML
	private JFXComboBox<Label> cb_name;

	public void initialize() {
		cb_option.getItems().add(new Label(city));
		cb_option.getItems().add(new Label(region));
		cb_option.getItems().add(new Label(district));
	}

	public void setMainScreen(MainScreenCont mainScreenCont) {
		this.mainScreenCont = mainScreenCont;

	}

	public void onOptionChoosen() {
		ResultSet result = null;
		try {
			switch (cb_option.getValue().getText()) {
			case city:
				result = DatabaseConnector.query("SELECT postcode FROM city");
				break;
			case district:
				result = DatabaseConnector.query("SELECT district FROM district");
				break;
			case region:
				result = DatabaseConnector.query("SELECT region FROM region");
				break;

			default:
				break;
			}
			cb_name.getItems().clear();
			while (result.next()) {
				int i = 1;
				while (i <= result.getMetaData().getColumnCount()) {
					cb_name.getItems().add(new Label(result.getString(i)));
					i++;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setStage(Stage stage) {
		this.stage = stage;

	}

	public void submit() {
		mainScreenCont.showCrimesByArea(cb_option.getValue().getText(), cb_name.getValue().getText());
		stage.close();
	}

}
