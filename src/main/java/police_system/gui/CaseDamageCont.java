package police_system.gui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class CaseDamageCont {

	private MainScreenCont mainScreenCont;
	public static final String unresolved = "UNRESOLVED";
	public static final String resolved = "RESOLVED";
	private Stage stage;
	@FXML
	private JFXButton b_submit;
	@FXML
	private JFXTextField f_cases;
	@FXML
	private JFXComboBox<Label> cb_options;

	public void initialize() {
		cb_options.getItems().add(new Label(unresolved));
		cb_options.getItems().add(new Label(resolved));
	}

	public void setMainScreen(MainScreenCont mainScreenCont) {
		this.mainScreenCont = mainScreenCont;

	}

	public void setStage(Stage stage) {
		this.stage = stage;

	}

	public void submit() {
		mainScreenCont.showCaseDamage(cb_options.getValue().getText());
		stage.close();
	}

}
