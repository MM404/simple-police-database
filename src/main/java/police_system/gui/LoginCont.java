package police_system.gui;

import java.io.IOException;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import police_system.database.DatabaseConnector;

/**
 * Controler class for login screen
 * 
 * @author Mat�� Mah�t
 *
 */

public class LoginCont {

	@FXML
	private JFXTextField linuxName;
	@FXML
	private JFXPasswordField linuxPassword;
	@FXML
	private JFXTextField mysqlName;
	@FXML
	private JFXPasswordField mysqlPassword;
	@FXML
	private JFXCheckBox sameAccount;
	@FXML
	private JFXButton login;
	@FXML
	private JFXButton esc;
	@FXML
	private Label errorLabel;

	public void initialize() {
		linuxName.setStyle("-fx-text-fill: #f1f8e9");
		linuxPassword.setStyle("-fx-text-fill: #f1f8e9");
		mysqlName.setStyle("-fx-text-fill: #f1f8e9");
		mysqlPassword.setStyle("-fx-text-fill: #f1f8e9");
	}

	public void sameAccountAction() {
		if (sameAccount.isSelected()) {
			mysqlName.setDisable(true);
			mysqlPassword.setDisable(true);
		} else {
			mysqlName.setDisable(false);
			mysqlPassword.setDisable(false);
		}
	}

	public void login() {
		if (sameAccount.isSelected()) {
			mysqlName.setText(linuxName.getText());
			mysqlPassword.setText(linuxPassword.getText());
		}

		try {
			DatabaseConnector.connect(linuxName.getText(), linuxPassword.getText(), mysqlName.getText(),
					mysqlPassword.getText());

			close();
			start();
		} catch (Exception e) {
			errorLabel.setText("Something is rotten, try again!");
		}

	}

	private void start() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MainView.fxml"));
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene(fxmlLoader.getRoot(), 1200, 600);
		Stage primaryStage = new Stage();
		primaryStage.setTitle("Police Station");
		primaryStage.setScene(scene);
		primaryStage.initStyle(StageStyle.UNDECORATED);
		primaryStage.setMaximized(false);
		primaryStage.show();
		primaryStage.setResizable(false);

		// ((Controller)
		// (fxmlLoader.getController())).setSessionManager(sessionManager);
	}

	public void escDragEntered() {
		esc.setStyle("-fx-background-color: #c12b00");
	}

	public void escDragExited() {
		esc.setStyle("-fx-background-color: #2962ff");
	}

	public void close() {
		// get a handle to the stage
		Stage stage = (Stage) esc.getScene().getWindow();
		// close window
		stage.close();
	}

}
