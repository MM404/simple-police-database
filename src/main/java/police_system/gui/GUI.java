package police_system.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * GUI initialization and closing handler
 * 
 * @author Mat�� Mah�t
 *
 */
public class GUI extends Application {
	private Stage primaryStage;

	/**
	 * first method called when GUI is initialized
	 * 
	 * @param stage
	 *            - primary Stage(context)
	 */
	@Override
	public void start(Stage stage) throws Exception {

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("login.fxml"));
		fxmlLoader.load();
		Scene scene = new Scene(fxmlLoader.getRoot(), 600, 400);
		primaryStage = stage;
		primaryStage.setTitle("Police Station Login");
		primaryStage.setScene(scene);
		primaryStage.initStyle(StageStyle.UNDECORATED);
		primaryStage.setResizable(false);
		primaryStage.show();
	}

	public void stop() {
	}
}
