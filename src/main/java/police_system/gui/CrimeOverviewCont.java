package police_system.gui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;

import javafx.fxml.FXML;
import javafx.stage.Stage;

public class CrimeOverviewCont {

	private MainScreenCont mainScreenCont;
	private Stage stage;
	@FXML
	private JFXButton b_submit;
	@FXML
	private JFXDatePicker dp_from;
	@FXML
	private JFXDatePicker dp_to;

	public void initialize() {

	}

	public void setMainScreen(MainScreenCont mainScreenCont) {
		this.mainScreenCont = mainScreenCont;

	}

	public void setStage(Stage stage) {
		this.stage = stage;

	}

	public void submit() {
		mainScreenCont.showCrimeOverview(dp_from.getValue(), dp_to.getValue());
		stage.close();
	}

}
