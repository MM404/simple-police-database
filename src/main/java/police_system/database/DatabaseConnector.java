package police_system.database;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

/**
 * ORACLE connection handler
 * 
 * @author Mat�� Mah�t
 *
 */
public class DatabaseConnector {
	// constants
	private static final String host = "cezar.fri.uniza.sk";
	private static final String url = "jdbc:oracle:thin:@//asterix.fri.uniza.sk:1521/orclpdb.fri.uniza.sk";
	private static String userName = "";
	private static java.sql.Connection conn;
	private static Session session;

	/**
	 * Connect to linux server(using SSH) and ORACLE database
	 * 
	 * @param serverUser
	 * @param serverPass
	 * @param dbUser
	 * @param dbPass
	 * @throws Exception
	 */
	public static void connect(String serverUser, String serverPass, String dbUser, String dbPass) throws Exception {
		if (!userName.isEmpty()) {
			System.out.println("co si jaaaak");
			return;
		}

		// Set StrictHostKeyChecking property to no to avoid UnknownHostKey issue
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		JSch jsch = new JSch();
		session = jsch.getSession(serverUser, host, 22);
		session.setPassword(serverPass);
		session.setConfig(config);
		session.connect();
		conn = DriverManager.getConnection(url, dbUser, dbPass);
		System.out.println("Database connection established");

		userName = serverUser;

	}

	/**
	 * Disconnect from linux server(using SSH) and database
	 * 
	 * @throws SQLException
	 */
	public static void disconnect() throws SQLException {
		if (conn != null && !conn.isClosed()) {
			System.out.println("Closing Database Connection");
			conn.close();
			System.out.println("DB CONNECTION CLOSED");
		}
		if (session != null && session.isConnected()) {
			System.out.println("Closing SSH Connection");
			session.disconnect();
			System.out.println("SSH CONNECTION CLOSSED");
		}
	}

	/**
	 * Getter for local users
	 * 
	 * @return user
	 */
	public static String getUser() {
		return userName;
	}

	/**
	 * Send query to database
	 * 
	 * @param query
	 *            - sql statement
	 * @return result set
	 * @throws SQLException
	 */
	public static ResultSet query(String query) throws SQLException {
		return conn.createStatement().executeQuery(query);
	}

	/**
	 * Execute update on database.
	 * 
	 * @param sql
	 * @throws SQLException
	 */
	public static void update(String sql) throws SQLException {
		conn.createStatement().executeUpdate(sql);
	}

	public static CallableStatement prepareCall(String sql) throws SQLException {
		return conn.prepareCall(sql);
	}

	public static Connection getConnection() {
		return conn;
	}

}
